import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/retry';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class UsersService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  getUsers(): Promise<Array<any>> {
    // Make the HTTP request: get all users
    return this.http.get('https://jsonplaceholder.typicode.com/users')
      .toPromise()
      .then((response) => {
        return response;
      })
      .catch(this.handleError);
  }

  getUser(id): Promise<any> {
    return this.getUsers()
      .then(users => users.find(user => user.id === id));
  }

  setUserLoggedIn(user, clear = false) {
    if (clear) {
      sessionStorage.removeItem('isUserLoggedIn');
    } else {
      sessionStorage.setItem('isUserLoggedIn', JSON.stringify(user));
    }
  }

  getUserLoggedIn() {
    return JSON.parse(sessionStorage.getItem('isUserLoggedIn'));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
