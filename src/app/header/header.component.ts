import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user = null;

  constructor(
    private router: Router,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.user = this.usersService.getUserLoggedIn();
  }

  logoutLink() {
    const loggedUser = this.usersService.getUserLoggedIn();
    this.usersService.setUserLoggedIn(loggedUser, true);
    this.router.navigate(['/']);
  }
}
