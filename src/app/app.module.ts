import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { CollapseModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersService } from './users.service';
import { AuthguardGuard } from './authguard.guard';
import { TodosComponent } from './todos/todos.component';
import { AlbumsComponent } from './albums/albums.component';
import { PostsComponent } from './posts/posts.component';

const appRoutes = [
  {
    path: '',
    component: LoginFormComponent,
    pathMatch: 'full'
  },
  {
    path: 'dashboard/:userId',
    component: DashboardComponent,
    canActivate: [AuthguardGuard],
    pathMatch: 'full'
  },
  {
    path: 'posts',
    component: PostsComponent,
    canActivate: [AuthguardGuard],
    pathMatch: 'full'
  },
  {
    path: 'todos',
    component: TodosComponent,
    canActivate: [AuthguardGuard],
    pathMatch: 'full'
  },
  {
    path: 'albums',
    component: AlbumsComponent,
    canActivate: [AuthguardGuard],
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginFormComponent,
    DashboardComponent,
    TodosComponent,
    AlbumsComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    CollapseModule.forRoot(),
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    UsersService,
    AuthguardGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
