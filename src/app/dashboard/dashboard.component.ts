import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @Input() user: any;
  navigated = false; // true if navigated here
  private selectedId: number;

  constructor(
    private route: ActivatedRoute,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      if (params['userId'] !== undefined) {
        const userId = +params['userId'];
        this.navigated = true;
        this.usersService.getUser(userId)
            .then(user => this.user = user);
      } else {
        this.navigated = false;
        this.user = {};
      }
    });
  }

}
