import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  users: any;
  error = false;
  errmessage = '';
  userLoggedIn = null;

  constructor(
    private router: Router,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.users = this.usersService.getUsers()
      .then(
        users => {
          this.users = users;

          this.userLoggedIn = this.usersService.getUserLoggedIn();

          if (this.userLoggedIn) {
            const user = this.userExists(this.userLoggedIn.email);

            if (user) {
              this.router.navigate(['dashboard', user.id]);
            }
          }
        }
      );
  }

  userExists(userEmail) {
    const filteredValue = this.users.filter(
      function(item) {
        return item.email === userEmail;
      }
    );

    return filteredValue.length > 0 ? filteredValue[0] : false;
  }

  gotoDashboard(e) {
    const userEmail = e.target.elements[0].value;
    const user = this.userExists(userEmail);

    this.usersService.setUserLoggedIn(user);

    if (user) {
      this.error = false;
      this.router.navigate(['dashboard', user.id]);
    } else {
      e.preventDefault();
      this.error = true;
      this.errmessage = 'User Not Found!';
    }
  }
}
